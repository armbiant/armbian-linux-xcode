# Changelog

## 2.0.0 Release notes (30-11-2022)

### Breaking Changes
- The repository can be now used as a submodule in all app projects.
- Tuist version raised to 3.14.


## 1.2.1 Release notes (15-11-2021)

### Enhancements
- Added option to specify test plan paths.


## 1.2.0 Release notes (23-09-2021)

### Breaking Changes
- Added support to XCode 13 by updating tuist version to 1.51.0
- Changes in Project spec required for working with version 1.51.0


## 1.1.0 Release notes (26-01-2021)

### Breaking Changes
- Product name was changed to display name to avoid bad resolved permissions due to newer Xcode parsing method which doesn't accept special characters anymore (since iOS 14).

### Enhancements
- Required Tuist version defined.
- Added option to define supported devices (iPhone only is set as default option).

### Bugfixes
- The dynamic build version format changed to only contain numbers since special characters are not allowed when uploading on AppStore.


## 1.0.0 Release notes (30-06-2020)

### Breaking Changes
- Initial state for creating common projects as used in SYNETECH.

### Enhancements
- Added option to assign different App icon sets for each flavour.
- Added script for auto-registering dependencies to Container (described [here](https://www.notion.so/synetech/Generated-Container-Registrations-99aadcd2892f426b8f1d1c66eabd3326)).


## x.x.x Release notes (dd-mm-yyyy)

### Breaking Changes
### Deprecated
### Enhancements
### Bugfixes
