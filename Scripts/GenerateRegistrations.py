import os
import re
import sys

if os.environ['ENABLE_PREVIEWS'] == "YES":
   sys.exit(0)


""" CONFIGURATION """

OUTPUT_FILE_PATH = 'Sources/Generated/AutoRegister.swift'


""" TEMPLATES """

output_file_template = """
// AUTOGENERATED FILE - DO NOT EDIT!

import Swinject

protocol AutoRegisteredDataSource {}
protocol AutoRegisteredRepository {}
protocol AutoRegisteredUseCase {}
protocol AutoRegistereddInteractor {}

func autoregisterDataSource(to container: Container) {
    <AutoRegisteredDataSource>
}

func autoregisterRepositories(to container: Container) {
    <AutoRegisteredRepository>
}

func autoregisterUseCases(to container: Container) {
    <AutoRegisteredUseCase>
}

func autoregisterInteractors(to container: Container) {
    <AutoRegistereddInteractor>
}
"""

registration_call_template = """
    container.register(<TYPE_NAME>.self) { r in
        <TYPE_NAME>Impl(
            <PARAMS>
        )
    }
""".rstrip()

registration_type_to_source_files = {
    'AutoRegisteredDataSource': [],
    'AutoRegisteredRepository': [],
    'AutoRegisteredUseCase': [],
    'AutoRegistereddInteractor': []
}

output_file_name = OUTPUT_FILE_PATH.split('/')[-1]
output_folder = OUTPUT_FILE_PATH.replace(output_file_name, '')[:-1]

for dirpath, _, filenames in os.walk(os.getcwd()):
    for filename in filenames:
        if output_file_name in filename:
            continue
        if '.swift' in filename:
            with open(dirpath + os.sep + filename, 'r') as file:
                try:
                    content = file.read()
                    for key in registration_type_to_source_files:
                        if (key) in content:
                            registration_type_to_source_files[key].append(content)
                except UnicodeDecodeError:
                    continue

def process_content(content):
    # Get type
    auto_registrated_type_matches = re.findall('(\w+):.*AutoRegistered', content)
    if not auto_registrated_type_matches:
        return ''
    auto_registrated_type = auto_registrated_type_matches[0]
    auto_registrated_protocol = re.sub('Impl', '', auto_registrated_type)

    # Get init params
    class_definition = re.findall('(class ' + auto_registrated_type + '([\s\S]*?\n}))', content)[0][0]
    init_params_matches = re.findall('init\(([\s\S]*?)\)', class_definition)
    if not init_params_matches:
        init_params = []
    else:
        init_params = init_params_matches[0].replace('\n', '').split(',')
    init_params = [param.strip() for param in init_params]

    # Process template
    processed_template = registration_call_template.replace('<TYPE_NAME>', auto_registrated_protocol)
    param_rows = []
    for param in init_params:
        param_name = param.split(':')[0]
        param_type = param.split(': ')[1]
        param_rows.append(param_name + ': r.resolve(' + param_type + '.self)!')
    params_separator = ',\n            '
    if not param_rows:
        processed_template = re.sub('\(\n.*<PARAMS>\n.*\)', '()', processed_template)
    else:
        processed_template = processed_template.replace('<PARAMS>', params_separator.join(param_rows))
    return processed_template

processed_output = output_file_template

for key in registration_type_to_source_files:
    processed_files = [process_content(file) for file in registration_type_to_source_files[key]]
    placeholder = '<' + key + '>'
    if not registration_type_to_source_files[key]:
        processed_output = processed_output.replace(placeholder, '')
    else:
        processed_output = processed_output.replace(placeholder, '\n'.join(processed_files))
try:
    with open(OUTPUT_FILE_PATH, 'w') as file:
        file.write(processed_output)
except:
    print("The output folder " + output_folder + " doesn't exist.")
    print("Create it or change OUTPUT_FILE_PATH in the script.")
    sys.exit(1)

