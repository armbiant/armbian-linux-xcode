import os
import re
import sys

if os.environ['ENABLE_PREVIEWS'] == "YES":
    sys.exit(0)


""" CONFIGURATION """
OUTPUT_FILE_PATH = 'Sources/Generated/AutoInit.swift'
""" TEMPLATE """
init_template = """
    // MARK: - Init
    init(<INIT_ARGUMENTS>) {
        <PROPERTY_ASSIGNMENTS>
    }
""".rstrip()
# Create file with AutoInit protocol
auto_init_file_content = """
// GENERATED FILE - DO NOT EDIT
protocol AutoInit {}
"""
try:
    with open(OUTPUT_FILE_PATH, 'w') as file:
        file.write(auto_init_file_content)
except:
    output_file_name = OUTPUT_FILE_PATH.split('/')[-1]
    output_folder = OUTPUT_FILE_PATH.replace(output_file_name, '')[:-1]
    print("The output folder " + output_folder + " doesn't exist.")
    print("Create it or change OUTPUT_FILE_PATH in the script.")
    sys.exit(1)
def process_target_file(file_path, content):
    print("Processing: " + file_path)
    if 'Sources' not in file_path:
        return
    # Get properties to initialize
    processed_template = init_template
    class_definition_matches = re.findall('(class ([\s\S]*?\n}))', content)
    if not class_definition_matches:
        return
    class_definition = class_definition_matches[0][0]
    properties = re.findall('private let .*', class_definition)
    properties_to_init = [prop for prop in properties if ('=' not in prop and '!' not in prop)]
    # Process template - init arguments
    init_arguments_list = [re.sub('private let ', '', prop) for prop in properties_to_init]
    init_arguments_separator = ',\n         '
    init_arguments = init_arguments_separator.join(init_arguments_list)
    processed_template = processed_template.replace('<INIT_ARGUMENTS>', init_arguments)
    # Process template - property assignments
    property_names = [argument.split(':')[0] for argument in init_arguments_list]
    property_assignments = [('self.' + prop + ' = ' + prop) for prop in property_names]
    property_assignment_separator = '\n        '
    processed_template = processed_template.replace('<PROPERTY_ASSIGNMENTS>',
                                                    property_assignment_separator.join(property_assignments))
    processed_template_rows = processed_template.split('\n')
    # Get last property row line number
    rows = content.split('\n')
    stripped_rows = [row.strip() for row in rows]
    last_property_row_line_number = stripped_rows.index(properties_to_init[-1])
    # Create new init
    result_rows = rows[:last_property_row_line_number+1] \
        + processed_template_rows \
        + rows[last_property_row_line_number+1:]
    result = '\n'.join(result_rows)
    if 'init' in class_definition:
        # Regenerate init
        original_init_block = re.findall('init\([\s\S]*?\}', content)[0]
        init_block = re.findall('init\([\s\S]*?\}', result)[0]
        last_assignment_row = re.findall('self\..*=.*', original_init_block)[-1]
        part_after_assignment = re.findall((last_assignment_row + '\n' + '([\s\S]*})'), original_init_block)[0]
        part_after_assignment_rows = part_after_assignment.split('\n')
        original_init_block_rows = original_init_block.split('\n')
        original_init_block_rows_stripped = [row.strip() for row in original_init_block_rows]
        init_block_rows = init_block.split('\n')
        init_block_rows_stripped = [row.strip() for row in init_block_rows]
        last_assignment_row_index = original_init_block_rows_stripped.index(last_assignment_row)
        regenerated_init_rows = init_block_rows[:-1] + part_after_assignment_rows
        regenerated_init = '\n'.join(regenerated_init_rows)
        result = content.replace(original_init_block, regenerated_init)
    with open(file_path, 'w') as file:
        file.write(result)
for dirpath, _, filenames in os.walk(os.getcwd()):
    for filename in filenames:
        if '.swift' in filename:
            file_path = dirpath + os.sep + filename
            try:
                with open(file_path, 'r') as file:
                    content = file.read()
                    if 'AutoInit' in content and not 'protocol AutoInit' in content:
                        process_target_file(file_path, content)
            except UnicodeDecodeError:
                continue
