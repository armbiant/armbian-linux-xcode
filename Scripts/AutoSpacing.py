import sys
import glob
import re

if len(sys.argv) != 2:
    print("Specify folder as an argument")
    exit()

for file in glob.glob(sys.argv[1] + '/**/*.swift', recursive=True):
    openedFile = open(file, "r+")
    lines = openedFile.readlines()
    writeLines = []

    for lineNumber in range(len(lines)):
        writeLines.append(lines[lineNumber])

        if lineNumber < len(lines) - 1:
            if re.search('^[\w\s]*(extension|class).*$', lines[lineNumber]) and not re.search('^\s*$', lines[lineNumber+1]):
                writeLines.append("\n")

    openedFile.seek(0)
    openedFile.writelines(writeLines)
    openedFile.close()
