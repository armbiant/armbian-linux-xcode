VERSION=1.9.0
export PATH="/opt/homebrew/bin:$PATH"
if which mint >/dev/null; then
    if mint which krzysztofzablocki/Sourcery@$VERSION 2>&1 | grep -q 'not installed'; then
        echo "warning: You're missing the desired Sourcery version ($VERSION) - you've probably forgot to run \`mint bootstrap\` in the root"
    else
        xcrun --sdk macosx mint run krzysztofzablocki/Sourcery@$VERSION sourcery --sources Sources --templates tuist/Templates --output Sources/Generated --args MODULE=${PRODUCT_MODULE_NAME}
    fi
else
    echo "warning: Mint not installed, install by \`brew install mint\`"
fi