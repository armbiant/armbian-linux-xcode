import os

def checkPrivateExtensionModifiers(path, name, message):
    file = open(path)
    rows = file.readlines()

    extensionStarted = False
    privateExtension = []

    for row in rows:
        if row.startswith("private extension"):
            extensionStarted = True
        if extensionStarted:
            privateExtension.append(row)
            if row == "}\n":
                extensionStarted = False
                del privateExtension[0]
                privateExtension.pop()
                for line in privateExtension:
                    if "private" in line:
                        row_index = rows.index(line) + 1
                        os.system('echo '+path+':'+str(row_index)+':1: warning: '+name+' Violation: '+message+' \('+name.lower().replace(" ", "_")+'\).\n')
                del privateExtension[:]


def checkMarksOrder(path, orderedCheckedValues, name, message):
    file = open(path)
    rows = file.readlines()
    marks = []
    for row in rows:
        for mark in orderedCheckedValues:
            if mark in row:
                marks.append(mark)
    if marks:
        checkedMarks = [mark for mark in orderedCheckedValues if mark in marks]
        for index in range(len(checkedMarks)):
            if marks[index] != checkedMarks[index]:
                wrong_row = [row for row in rows if marks[index] in row][0]
                row_index = rows.index(wrong_row) + 1
                os.system('echo '+path+':'+str(row_index)+':1: warning: '+name+' Violation: '+message+' \('+name.lower().replace(" ", "_")+'\).\n')


orderedCheckedMarksNotIndented = ['// MARK: - Data binding',
                                  '// MARK: - Interaction binding',
                                  '// MARK: - Delegation handling']

orderedCheckedMarksIndented = ['// MARK: - Aliases',
                               '// MARK: - Views',
                               '// MARK: - Properties',
                               '// MARK: - Init',
                               '// MARK: - Lifecycle',
                               '// MARK: - Setup']

for current_path, folders, files in os.walk('./Sources/'):
    for file in files:
        path = os.path.join(current_path, file)
        absolutePath = os.path.abspath(path)
        if '.swift' in absolutePath:
            checkMarksOrder(absolutePath, orderedCheckedMarksNotIndented, "Extensions order", "Extensions should be ordered like - Data binding, Interaction binding, Delegation handling")
            checkMarksOrder(absolutePath, orderedCheckedMarksIndented, "Type order", "Types must be ordered")
            checkPrivateExtensionModifiers(absolutePath, "Private extension access modifier", "Remove access modifier, it should be only in extension")
