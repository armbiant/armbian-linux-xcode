VERSION=7.1.0
export PATH="/opt/homebrew/bin:$PATH"
if which mint >/dev/null; then
    if mint which mac-cain13/R.swift@$VERSION 2>&1 | grep -q 'not installed'; then
        echo "warning: You're missing the desired R.swift version ($VERSION) - you've probably forgot to run \`mint bootstrap\` in the root"
    else
        xcrun --sdk macosx mint run mac-cain13/R.swift@$VERSION rswift generate "$SCRIPT_OUTPUT_FILE_0" --generators image,string,color,file,font
    fi
else
    echo "warning: Mint not installed, install by \`brew install mint\`"
fi