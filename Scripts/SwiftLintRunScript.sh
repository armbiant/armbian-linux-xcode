VERSION=0.50.1
export PATH="/opt/homebrew/bin:$PATH"
if which mint >/dev/null; then
    if mint which realm/SwiftLint@$VERSION 2>&1 | grep -q 'not installed'; then
        echo "warning: You're missing the desired Swiftlint version ($VERSION) - you've probably forgot to run \`mint bootstrap\` in the root"
    else
        xcrun --sdk macosx mint run realm/SwiftLint@$VERSION swiftlint --config tuist/.swiftlint.yml
    fi
else
    echo "warning: Mint not installed, install by \`brew install mint\`"
fi