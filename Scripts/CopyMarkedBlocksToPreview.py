import os
import re

"""
Expectations:

- There are some files,
  that contain either '// MARK: <SOMETHING> + Preview' above some extensions
  or '// Preview' above some functions.

- For those entities, there is a file somewhere,
  that has the name as the entity, except with 'Preview' instead of 'Impl'.


Processing:

For all above mentioned files, the following is done:
    - Every marked extension and function is extracted
    - Every extension is either copied into the preview file, or it replaces the current one
    - All functions are aggregated into an extension marked with // MARK: - Preview,
      which is either copied into the preview file, or it replaces the current one

"""


if "ENABLE_PREVIEWS" in os.environ and os.environ["ENABLE_PREVIEWS"] == "YES":
    raise SystemExit


PREVIEW_EXTENSION_MARK_SUFFIX = "+ Preview"
PREVIEW_FUNCTION_COMMENT = "// Preview"
PREVIEW_FUNCTIONS_EXTENSION_MARK = "// MARK: - Preview"


all_swift_file_paths_to_content_map = {}
targe_swift_file_path_to_content_map = {}

for directory, directories, files in os.walk(os.getcwd()):
    if "Sources" not in directory or "BuildTools" in directory:
        continue
    for file in files:
        if ".swift" in file:
            path = directory + os.sep + file
            with open(path, "r") as file:
                try:
                    content = file.read()
                    all_swift_file_paths_to_content_map[path] = content
                    if (
                        PREVIEW_FUNCTION_COMMENT in content
                        or PREVIEW_EXTENSION_MARK_SUFFIX in content
                    ):
                        targe_swift_file_path_to_content_map[path] = content
                except UnicodeDecodeError:
                    continue

file_paths = list(all_swift_file_paths_to_content_map.keys())

for file_path in targe_swift_file_path_to_content_map:

    content = targe_swift_file_path_to_content_map[file_path]
    entity_name = re.findall("class (\w*)", content)[0]
    preview_entity_name = entity_name.replace("Impl", "Preview")
    preview_extensions = re.findall(
        "// MARK: .* \\" + PREVIEW_EXTENSION_MARK_SUFFIX + "\n[\s\S]+?\n}", content
    )

    preview_functions = re.findall(
        PREVIEW_FUNCTION_COMMENT + "\n[\s\S]+?\n    }", content
    )

    target_file_path = [path for path in file_paths if preview_entity_name in path][0]
    target_file_content = all_swift_file_paths_to_content_map[target_file_path]

    for extension in preview_extensions:
        extension_mark = re.findall(
            "// MARK: .* \\" + PREVIEW_EXTENSION_MARK_SUFFIX, extension
        )[0]
        extension_mark = extension_mark.replace(entity_name, preview_entity_name)
        preview_extension = extension.replace(entity_name, preview_entity_name)
        if extension_mark in target_file_content:
            target_file_content = re.sub(
                extension_mark + "\n[\s\S]+?\n}",
                preview_extension,
                target_file_content,
            )
        else:
            target_file_content += "\n" + preview_extension

    if preview_functions:
        preview_functions_extension = (
            PREVIEW_FUNCTIONS_EXTENSION_MARK
            + "\nextension "
            + preview_entity_name
            + " {\n"
        )

        for function in preview_functions:
            function = function.replace(PREVIEW_FUNCTION_COMMENT, "")
            preview_functions_extension += function + "\n"

        preview_functions_extension += "\n}"

        if PREVIEW_FUNCTIONS_EXTENSION_MARK in target_file_content:
            target_file_content = re.sub(
                PREVIEW_FUNCTIONS_EXTENSION_MARK + "\n[\s\S]+?\n}",
                preview_functions_extension,
                target_file_content,
            )
        else:
            target_file_content += "\n\n" + preview_functions_extension

    with open(target_file_path, "w") as file:
        file.write(target_file_content)
