import ProjectDescription

/// Creates `Project` instance, which is directly used for generating the project file.
/// - Parameters:
///   - name: Name of the app.
///   - minimalOSVersion: Minimal iOS version, which should the app support.
///   - devices: Supported devices. Default is `.iphone`
///   - packages: List of all Swift packages, which will the project use.
///   - targetSpecs: List of targets specs.
///   - dependencies: Common dependencies, which should be added to all targets.
///   - testTargets: List of testing targets specs
/// - Returns: Casted `Project` instance.
public func createProject(name: String, minimalOSVersion: String,
                          devices: DeploymentDevice = .iphone, packages: [Package],
                          targetSpecs: [TargetSpec], dependencies: [TargetDependency],
                          testTargets: [TestTargetSpec] = []) -> Project {
    let targets = targetSpecs.map { $0.getTarget(dependencies: dependencies,
                                                 minOSVersion: minimalOSVersion,
                                                 devices: devices) }
        + testTargets.map { $0.getTarget(appName: name,
                                         minOSVersion: minimalOSVersion,
                                         devices: devices) }
    let schemes = targetSpecs.map { targetSpec -> Scheme in
        let testTargetsForCurrentTarget = testTargets.filter { $0.target == targetSpec.name }
        let testTargetsNames = testTargetsForCurrentTarget.map { $0.name }
        let testTargetsPlans = testTargetsForCurrentTarget.compactMap { $0.testPlans }.flatMap { $0 }
        return targetSpec.getScheme(
            testTargets: testTargetsNames.isEmpty ? nil : testTargetsNames,
            testPlans: testTargetsPlans
        )
    }
    return Project(
        name: name,
        organizationName: "SYNETECH",
        packages: packages,
        settings: Settings(
            base: SettingsDictionary()
                .appleGenericVersioningSystem()
        ),
        targets: targets,
        schemes: schemes,
        additionalFiles: ["Entitlements/**"]
    )
}