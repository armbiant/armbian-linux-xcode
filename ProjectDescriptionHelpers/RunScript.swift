import ProjectDescription

/// Enumerates all common run scripts, which can be used during the apps build.
public enum RunScript: CaseIterable {

    case rSwift
    case autoSpacing
    case swiftLint
    case markOrderPrivateExtension
    case generateInits
    case generateRegistrations
    case copyToPreviews
    case runSourcery

    /// Creates Tuist `TargetScript` type for the `RunScript` case.
    public var targetScript: TargetScript {
        switch self {
        case.rSwift:
            return TargetScript.pre(
                path: "tuist/Scripts/RSwiftRunScript.sh",
                name: "R.swift",
                outputPaths: ["$(SRCROOT)/R.generated.swift"],
                basedOnDependencyAnalysis: false
            )
        case .autoSpacing:
            return TargetScript.pre(
                tool: "python3",
                arguments: ["${PROJECT_DIR}/tuist/Scripts/AutoSpacing.py Sources"],
                name: "Auto spacing",
                basedOnDependencyAnalysis: false
            )
        case .swiftLint:
            return TargetScript.pre(
                path: "tuist/Scripts/SwiftLintRunScript.sh",
                name: "SwiftLint",
                basedOnDependencyAnalysis: false
            )
        case .markOrderPrivateExtension:
            return TargetScript.pre(
                tool: "python3",
                arguments: ["${PROJECT_DIR}/tuist/Scripts/MarkOrder+PrivateExtensionLintRunScript.py"],
                name: "Python lints",
                basedOnDependencyAnalysis: false
            )
        case .generateInits:
            return TargetScript.pre(
                tool: "python3",
                arguments: ["${PROJECT_DIR}/tuist/Scripts/GenerateInits.py"],
                name: "Generate inits for AutoInit conformers",
                basedOnDependencyAnalysis: false
            )
        case .generateRegistrations:
            return TargetScript.pre(
                tool: "python3",
                arguments: ["${PROJECT_DIR}/tuist/Scripts/GenerateRegistrations.py"],
                name: "Generate registrations to Container",
                basedOnDependencyAnalysis: false
            )
        case .copyToPreviews:
            return TargetScript.pre(
                tool: "python3",
                arguments: ["${PROJECT_DIR}/tuist/Scripts/CopyMarkedBlocksToPreview.py"],
                name: "Copy marked blocks to preview implementation",
                basedOnDependencyAnalysis: false
            )
        case .runSourcery:
            return TargetScript.pre(
                path: "tuist/Scripts/SourceryRunScript.sh",
                name: "Run Sourcery",
                basedOnDependencyAnalysis: false
            )
        }
    }

    public var shouldRunOnCI: Bool {
        switch self {
        case .rSwift, .generateInits, .generateRegistrations, .runSourcery:
            return true
        case .swiftLint, .markOrderPrivateExtension, .autoSpacing, .copyToPreviews:
            return false
        }
    }
}
