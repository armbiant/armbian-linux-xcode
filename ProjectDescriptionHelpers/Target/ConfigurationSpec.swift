import Foundation
import ProjectDescription

/// Spec for target configuration
public struct ConfigurationSpec {

    // MARK: - Properties
    let signingIdentity: SigningIdentity
    let provisioningProfileName: String
    let developmentTeamId: String
    let isDebug: Bool
    let additionalCompileFlags: [String]?

    // MARK: - Init
    /// Defines target _Build Settings_.
    /// - Parameters:
    ///   - signingIdentity: Use `.developer` for debug or development versions. Use `.distribution` for every case, which is distributed as `.ipa`.
    ///   - provisioningProfileName: Name of the provisioning profile. You can assign it in the Xcode UI and then copy the value from Build Settings -> Signing -> Provisioning Profile.
    ///   - developmentTeam: ID of the development team. Default value is "RA935TF4A8", which is the MeguMethod enterprise account.
    ///   - isDebug: Defines whether the target should include `DEBUG` compile flag. Default value is `false`.
    ///   - additionalCompileFlags: Adds custom compile flags to the target. Default value is `nil`.
    public init(signingIdentity: SigningIdentity, provisioningProfileName: String,
                developmentTeamId: String = "RA935TF4A8", isDebug: Bool = false,
                additionalCompileFlags: [String]? = nil) {
        self.signingIdentity = signingIdentity
        self.provisioningProfileName = provisioningProfileName
        self.developmentTeamId = developmentTeamId
        self.isDebug = isDebug
        self.additionalCompileFlags = additionalCompileFlags
    }
}

// MARK: - Cast to Tuist type
public extension ConfigurationSpec {

    /// Casts internal spec to Tuist `Configuration` type.
    /// - Parameters:
    ///   - targetName: Name of the target, needed for creating compile flag.
    ///   - displayName: Sets the Display name.
    ///   - appIconId: Identifer of App icon set in `.xcassets`. If `nil` is passed, the default one is used. Default is `nil`.
    /// - Returns: Casted `Configuration` instance that can be passed right to the desired `Target`.
    func getConfigurationSettings(targetName: String,
                                  displayName: String,
                                  appIconId: String? = nil) -> SettingsDictionary {
        var settings = SettingsDictionary()
            .manualCodeSigning(identity: signingIdentity.rawValue,
                               provisioningProfileSpecifier: provisioningProfileName)
            .otherSwiftFlags("$(inherited)", "-D", targetName.uppercased())
        settings[BuildSetting.developmentTeam.rawValue] = .string(developmentTeamId)
        settings[BuildSetting.displayName.rawValue] = .string(displayName)
        settings[BuildSetting.timestamp.rawValue] = .string(getTimestamp())
        if let appIconId = appIconId {
            settings[BuildSetting.appIcon.rawValue] = .string(appIconId)
        }
        if let additionalCompileFlags = additionalCompileFlags {
            var initialCompileFlags: [String] = []
            if isDebug {
                initialCompileFlags += [BuildSettingsConstant.debugCompileFlag.rawValue]
            }
            settings[BuildSetting.compilationConditions.rawValue]
                = .array(initialCompileFlags + additionalCompileFlags)
        }
        return settings
    }

    private func getTimestamp() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmm"
        return dateFormatter.string(from: Date())
    }
}
