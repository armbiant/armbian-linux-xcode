//
//  BuildSettingsConstant.swift
//  ProjectDescriptionHelpers
//
//  Created by Lukáš Růžička on 20/08/2020.
//

/// Common values for _Build Settings_ fields
public enum BuildSettingsConstant: String {

    /// Compile flag for Debug versions
    case debugCompileFlag = "DEBUG"
}
