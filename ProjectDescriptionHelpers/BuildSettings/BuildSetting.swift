//
//  BuildSetting.swift
//  Manifests
//
//  Created by Lukáš Růžička on 20/08/2020.
//

/// Keys for _Build Settings_ fields
public enum BuildSetting: String {

    /// Custom key for the app name (visible to the user) needs to be referenced from `Info.plist`.
    case displayName = "DISPLAY_NAME"
    /// Team ID used for signing provisioning profiles
    case developmentTeam = "DEVELOPMENT_TEAM"
    /// Custom compilation flags
    case compilationConditions = "SWIFT_ACTIVE_COMPILATION_CONDITIONS"
    /// Path to hosting target, which should be tested
    case testHost = "TEST_HOST"
    /// App icon set identifier
    case appIcon = "ASSETCATALOG_COMPILER_APPICON_NAME"
    /// Custom key with timestamp of project generating
    case timestamp = "PROJECT_GENERATED_TIMESTAMP"
}
